import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {Onboarding} from "./components/Pages/Onboarding/Onboarding";
import Home from "./components/Pages/Home/Home";
import Adopcion from './components/Pages/Adopcion/Adopcion';
import {Splash} from "./components/Pages/Splash/CompomentSplash/Splash";



function App() {
  return (

      <div className="App">
          <Router>
              <Switch>
                  <Route path="/onboarding">
                      <Onboarding/>
                  </Route>
                  <Route path="/home">
                     <Home/>
                  </Route>
                  <Route path="/adopcion">
                      <Adopcion/>
                  </Route>
                  <Route path="/filtros">
                      {/*<Chronologic/>*/}
                  </Route>
                  <Route path="/formadopcion">
                      {/*<Chronologic/>*/}
                  </Route>
                  <Route path="/">
                      <Splash/>
                  </Route>
              </Switch>

          </Router>
      </div>
  );
}

export default App;
