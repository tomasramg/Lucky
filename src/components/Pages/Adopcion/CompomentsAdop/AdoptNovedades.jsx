import React from "react";
import './AdoptNovedades.scss'


function AdoptNovedades() {

    const BlogNovedades = [
        {
            img: 'assets/gatito@3x.png',
            title: "Simba",
            city: "Madrid",
            text: ">1.5km",
            fav: true,
        },
        {
            img: 'assets/ca@3x.png',
            title: "Rocky",
            city: "Madrid",
            text: ">30km",
            fav: false,
        },
        {
            img: 'assets/conejo@3x.png',
            title: "Uli",
            city: "Madrid",
            text: ">57km",
            fav: true,
        }
    ]


    return (
        <section className="b-novedades">

            <p className="b-novedades__seccion-titulo">Animales en Adopción</p>
            {BlogNovedades.map((item, index) =>
                <div key={index} className="b-novedades__fondo ">
                    <div className="relativo">
                        <img className="b-novedades__img" src={item.img} alt={item.title}/>
                        <div className="corazon">
                        <img src="assets/iconos/favoritos.png"/>
                        <img src="assets/iconos/favoritosRelleno.png"/>
                    </div>
                    </div>
                    <div className="column ubicacion cajadeinfo">
                        <div className="row  ">
                            <div className="b-adopcion__ubicacion__titulo padingcajitanombre">{item.title}</div>
                            <div className="column padingcajitaanimal">
                                <div className="b-adopcion__ubicacion__ciudad padingcajitaanimal">{item.city}</div>
                                <div className="b-adopcion__ubicacion__texto padingcajitaanimal">{item.text}</div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </section>
    );
}

export default AdoptNovedades;