import React, {useState} from "react";
import FormAdoptionComponent1 from "./Formadoptioncomponents/Form1/FormAdoptionComponent1";
import FormAdoptionComponent2 from "./Formadoptioncomponents/Form2/FormAdoptionComponent2";
import FormAdoptionComponent3 from "./Formadoptioncomponents/Form3/FormAdoptionComponent3";
import axios from "axios";

function FormAdoption() {
    const [changeForm, setChangeForm] = useState(0);
    const [animalData, setAnimalData] = useState({});

    const handlerForm = (num) => {
        setChangeForm(num);
    }

    const addAnimalData = (newAnimalData) => {
        setAnimalData(Object.assign(animalData, newAnimalData));
        if (Object.keys(animalData).length > 18) {
            axios.post('http://localhost:3000/adoption', {animalData}).then(res => console.log("Successfully sent to server")).catch(e => console.log(e));
        }
    }


    return (
        <div>
            {changeForm === 0 && <FormAdoptionComponent1
                handlerForm={handlerForm}
                addAnimalData={addAnimalData}
            />}

            {changeForm === 1 && <FormAdoptionComponent2
                handlerForm={handlerForm}
                addAnimalData={addAnimalData}
            />}

            {changeForm === 2 && <FormAdoptionComponent3
                addAnimalData={addAnimalData}
            />}

        </div>
    );
}

export default FormAdoption;
