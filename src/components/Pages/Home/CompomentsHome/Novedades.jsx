import React from "react";
import './Novedades.scss'



function Novedades() {

    const BlogNovedades = [
        {
            img: process.env.PUBLIC_URL+'assets/iconos/hamster@3x.png',
            title: "10 Curiosidades sobre las chinchillas",
        },
        {
            img: process.env.PUBLIC_URL+'assets/iconos/lagarto@3x.png',
            title: "¿Sabes qué comen las iguanas?",
        },
        {
            img: process.env.PUBLIC_URL+'assets/iconos/PerroCasco@3x.png',
            title: "10 lugares para visitar con tu perro en Madrid",
        }
    ]


        return(
            <section className="b-novedades">
            <p className="b-novedades__seccion-titulo">Novedades</p>
            {BlogNovedades.map((item, index) =>
              <div key={index} className="b-novedades__fondo">
                  <img className="b-novedades__img" src={item.img} alt={item.title}/>
                  <p className="b-novedades__titulo">{item.title}</p>
              </div>
            )}
        </section>
    );
}

export default Novedades;