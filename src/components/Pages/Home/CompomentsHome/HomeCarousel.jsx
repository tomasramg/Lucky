import React from "react";
import {Carousel} from "react-responsive-carousel";
import "./HomeCarousel.scss"
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader


function HomeCarousel() {

    const homeCarouselOptions = [
        {
            icon: process.env.PUBLIC_URL + 'assets/iconos/donar@3x.png',
            title: "Ayuda a las mascotas",
            text: "Revisa el proceso de tus adopciones en curso"
        },
        {
            icon: process.env.PUBLIC_URL + 'assets/iconos/mascota@3x.png',
            title: "Estado de la adopción",
            text: "Revisa el proceso de tus adopciones en curso"
        },
        {
            icon: process.env.PUBLIC_URL + 'assets/iconos/apadrina@3x.png',
            title: "Aporta tu granito",
            text: "Revisa el proceso de tus adopciones en curso"
        }
    ];

    return (
        <Carousel className="c-carousel-home">
            {homeCarouselOptions.map((item, index) =>
                <div className="c-carousel-home__container" key={index}>
                    <img className="c-carousel-home__img" src={item.icon} alt="home"/>
                    <div className="c-carousel-home__content-box">
                        <p className="c-carousel-home__title">{item.title}</p>
                        <p className="c-carousel-home__text">{item.text}</p>
                    </div>
                </div>
            )}
        </Carousel>
    );
}

export default HomeCarousel;
